var cbpBGSlideshow = (function() {
	var $slideshow = $( '#cbp-bislideshow' ),
		$items = $slideshow.children( 'li' ),
		itemsCount = $items.length,
		$controls = $( '#cbp-bicontrols' ),
		navigation = {
			$navPrev : $controls.find( 'span.cbp-biprev' ),
			$navNext : $controls.find( 'span.cbp-binext' ),
			$navPlayPause : $controls.find( 'span.cbp-biplay' ),
			$navUp : $controls.find( 'span.cbp-biup' ),
			$navDown : $controls.find( 'span.cbp-bidown' )
		},
		current = 0,
		slideshowtime,
		isSlideshowActive = true,
		interval = 3500;

	function init( config ) {
		$slideshow.imagesLoaded( function() {
			$(".container").css("opacity","1");
			$(".loader").hide();
			if( Modernizr.backgroundsize ) {
				$items.each( function() {
					var $item = $( this );
					$item.css( 'background-image', 'url(' + $item.find( 'img' ).attr( 'src' ) + ')' );
				} );
			}
			else {
				$slideshow.find( 'img' ).show();
			}
			$items.eq( current ).css( 'opacity', 1 );
		} );
		
	}

	function initEvents() {
		navigation.$navPlayPause.on( 'click', function() {
			var $control = $( this );
			if( $control.hasClass( 'cbp-biplay' ) ) {
				$control.removeClass( 'cbp-biplay' ).addClass( 'cbp-bipause' );
				startSlideshow();
			}
			else {
				$control.removeClass( 'cbp-bipause' ).addClass( 'cbp-biplay' );
				stopSlideshow();
			}
		} );
		navigation.$navPrev.on( 'click', function() { 
			navigate( 'prev' );
			if( isSlideshowActive ) { 
				startSlideshow(); 
			} 
		} );
		navigation.$navNext.on( 'click', function() { 
			navigate( 'next' );
			if( isSlideshowActive ) { 
				startSlideshow(); 
			}
		} );
		navigation.$navUp.on( 'click', function() { 
			navigate( 'up' );
		} );
		navigation.$navDown.on( 'click', function() { 
			navigate( 'down' );
		} );

	}
	function navigate( direction ) {
		if( direction === 'next' ) {
			var $oldItem = $items.eq( current );
			current = current < itemsCount - 1 ? ++current : 0;
			var $newItem = $items.eq( current );
			$oldItem.css( 'opacity', 0 );
			$newItem.css( 'opacity', 1 );
		}
		else if( direction === 'prev' ) {
			var $oldItem = $items.eq( current );
			current = current > 0 ? --current : itemsCount - 1;
			var $newItem = $items.eq( current );
			$oldItem.css( 'opacity', 0 );
			$newItem.css( 'opacity', 1 );
		}
		else if(direction === 'up'){
			stopSlideshow();
			$(".container").hide();
			$(".loader").show();
			$(".product_1_1").attr("src","http://z.about.com/d/cameras/1/0/u/1/bigcat.JPG");
			$(".product_1_2").attr("src","http://www.wallsave.com/wallpapers/2560x2048/architecture-widescreen/752466/architecture-widescreen-hd-cat-752466.jpg");
			$(".product_1_3").attr("src","http://upload.wikimedia.org/wikipedia/commons/d/dc/HD_69830_Planet.jpg");
			$(".product_1_4").attr("src","http://www.letsgomobile.org/images/reviews/0175/hd-photos.jpg");
			init();
			$(".container").show();
		}
		else if(direction === 'down'){
			stopSlideshow();
			$(".container").hide();
			$(".loader").show();
			$(".product_1_1").attr("src","images/products/1/1.jpg");
			$(".product_1_2").attr("src","images/products/1/2.jpg");
			$(".product_1_3").attr("src","images/products/1/3.jpg");
			$(".product_1_4").attr("src","images/products/1/4.jpg");
			init();
			$(".container").show();
		}
	}

	function startSlideshow() {
		isSlideshowActive = true;
		clearTimeout( slideshowtime );
		slideshowtime = setTimeout( function() {
			navigate( 'next' );
			startSlideshow();
		}, interval );
	}
	function stopSlideshow() {
		isSlideshowActive = false;
		clearTimeout( slideshowtime );
	}
	return { init : init , stopSlideshow : stopSlideshow , initEvents : initEvents };
})();